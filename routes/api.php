<?php

use App\Http\Controllers\AdminOrderController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductDataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Admin
Route::post('/admin/PaidOrders',[AdminOrderController::class,'getPaidOrder']);
Route::post('/admin/order',[AdminOrderController::class,'getOrderByStatus']);


//Category
Route::get('/category',[CategoryController::class,'index']);
Route::post('/category/create',[CategoryController::class,'create']);

//Product
Route::get('/product',[ProductController::class,'index']);
Route::post('/product/create',[ProductController::class,'create']);
Route::post('/product/filler',[ProductController::class,'filterProduct']);

//ProductData
Route::get('/productData/{id}',[ProductDataController::class,'getDataByIdProduct']);
Route::post('/productData/create',[ProductDataController::class,'create']);

//Order
Route::get('/order/{id}',[OrderController::class,'getOrderById']);
Route::post('/order/create',[OrderController::class,'create']);
Route::post('/order/add',[OrderController::class,'addProductToCart']);
Route::post('/order/remove',[OrderController::class,'removeProductToCart']);
Route::post('/order/checkbill',[OrderController::class,'checkBill']);
Route::post('/order/address',[OrderController::class,'updateAddress']);
Route::delete('/order/delete/{id}',[OrderController::class,'delete']);

