<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
    ];

    public function product_data(){
        return $this->belongsTo(ProductData::class,'product_data_id');
    }

    public function order(){
        return $this->belongsTo(Order::class,'order_id');
    }
}
