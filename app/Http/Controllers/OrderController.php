<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\OrderDetailResource;
use App\Http\Resources\OrderCheckBillResource;

class OrderController extends Controller
{
    //
    public function create(Request $request){
        $validator = Validator($request->all(),[
            'name' => 'required|string|max:255',
            'address' => 'string|nullable',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ]);
        }

        $order = new Order();
        
        if($request->address){
            $order->address = $request->address;
        }

        $order->employee_name = $request->name;

        $order->save();

        return response()->json([
            'status' => true,
            'data' => [
                'order_id' => $order->id
            ]
        ]);
    }

    public function delete($id){
        $delete = Order::find($id);
        if($delete){
            if($delete->status == 1){
                return response()->json([
                    'status' => 300,
                    'message' => 'Order has Approved can not delete.'
                ]);
            }
            OrderDetail::where('order_id',$id)->delete();
            $delete->delete();
            return response()->json([
                'status' => 200,
                'message' => 'Delete Order successfuly.'
            ]);
        }else{
            return response()->json([
                'status' => 400,
                'message' => 'Can not delete Order.'
            ]);
        }
    }

    public function addProductToCart(Request $request){
        $validator = Validator::make($request->all(),[
            'product_data_id' => 'required|numeric|exists:product_data,id',
            'order_id' => 'required|numeric|exists:orders,id',
            'amount' => 'numeric|nullable'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'data' => $validator->errors()
            ]);
        }

        $order = Order::find($request->order_id);
        if($order->status == 0){

            $checkHaveProduct = OrderDetail::where([['order_id',$request->order_id],['product_data_id',$request->product_data_id]])->first();

            if($checkHaveProduct){
                
                if($request->amount){
                    $checkHaveProduct->amount = $checkHaveProduct->amount + $request->amount;
                    $checkHaveProduct->order->update([
                        'price' =>  $checkHaveProduct->order->price + ($checkHaveProduct->product_data->price*$request->amount)
                    ]);
                }else{
                    $checkHaveProduct->amount = $checkHaveProduct->amount + 1;
                    $checkHaveProduct->order->update([
                        'price' =>  $checkHaveProduct->order->price + $checkHaveProduct->product_data->price
                    ]);
                }
                
                $checkHaveProduct->save();

                return response()->json([
                    'status' => true,
                    'data' => new OrderDetailResource($checkHaveProduct)
                ]);
                
            }else{
                $orderDetail = new OrderDetail();
                $orderDetail->product_data_id = $request->product_data_id;
                $orderDetail->order_id = $request->order_id;
                if($request->amount){
                    $orderDetail->amount = $request->amount;
                    $orderDetail->order->update([
                        'price' =>  $orderDetail->product_data->price*$request->amount
                    ]);
                }else{
                    $orderDetail->amount = 1;
                    $orderDetail->order->update([
                        'price' =>  $orderDetail->product_data->price
                    ]);
                }
                $orderDetail->save();

                return response()->json([
                    'status' => true,
                    'data' => new OrderDetailResource($orderDetail)
                ]);
            }

        }else{
            return response()->json([
                'status' => false,
                'message' => 'Order is closed.'
            ]);
        }
    }

    public function removeProductToCart(Request $request){
        $validator = Validator::make($request->all(),[
            'product_data_id' => 'required|numeric|exists:product_data,id',
            'order_id' => 'required|numeric|exists:orders,id',
            'amount' => 'numeric|nullable'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'data' => $validator->errors()
            ]);
        }

        $order = Order::find($request->order_id);
        if($order->status == 0){

            $checkHaveProduct = OrderDetail::where([['order_id',$request->order_id],['product_data_id',$request->product_data_id]])->first();

            if($checkHaveProduct){

                if($request->amount > $checkHaveProduct->amount){
                    return response()->json([
                        'status' => false,
                        'message' => 'This product not enough in cart.'
                    ]);
                }
                
                if($request->amount){
                    $checkHaveProduct->amount = $checkHaveProduct->amount - $request->amount;
                    $checkHaveProduct->order->update([
                        'price' =>  $checkHaveProduct->order->price - ($checkHaveProduct->product_data->price*$request->amount)
                    ]);
                }else{
                    $checkHaveProduct->amount = $checkHaveProduct->amount - 1;
                    $checkHaveProduct->order->update([
                        'price' =>  $checkHaveProduct->order->price - $checkHaveProduct->product_data->price
                    ]);
                }

                if($checkHaveProduct->amount == 0){
                    $checkHaveProduct->delete();
                }else{
                    $checkHaveProduct->save();
                }

                return response()->json([
                    'status' => true,
                    'data' => new OrderDetailResource($checkHaveProduct)
                ]);
                
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Not found this product in cart.'
                ]);
            }

        }else{
            return response()->json([
                'status' => false,
                'message' => 'Order is closed.'
            ]);
        }
    }

    public function getOrderById($id){
        $order = Order::find($id);
        if($order){
            return response()->json([
                'status' => 200,
                'data' => new OrderCheckBillResource($order)
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Not found order.'
            ]);
        }

    }

    public function updateAddress(Request $request){
        $validator = Validator($request->all(),[
            'order_id' => 'required|numeric|exists:orders,id',
            'address' => 'required|string',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ]);
        }

        $order = Order::find($request->order_id);
        $order->address = $request->address;
        $order->save();

        return response()->json([
            'status' => true,
            'message' => 'Update Address successfuly.'
        ]);
    }

    public function checkBill(Request $request){
        $validator = Validator($request->all(),[
            'order_id' => 'required|numeric|exists:orders,id',
            'money' => 'required|numeric',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ]);
        }

        $order = Order::find($request->order_id);
        if($order->status == 0){

            if($request->money < $order->price){
                return response()->json([
                    'status' => 300,
                    'message' => 'Not enough money.'
                ]);
            }

            $amountMoney = $request->money - $order->price;
            $amountMoneyReturn = $request->money - $order->price;

            $list_money = [
                1000 => 0,
                500 => 0,
                100 => 0,
                50 => 0,
                20 => 0,
                10 => 0,
                5 => 0,
                1 => 0
            ];

            $listmoneyReturn = [];

            foreach($list_money as $key => $val){
                $list_money[$key] = floor($amountMoney/$key);
                if($list_money[$key] != 0){
                    $listmoneyReturn[$key] = floor($amountMoney/$key);
                }
                $amountMoney = $amountMoney%$key;
            }

            $order->status = 1;
            $order->save();
            
            return response()->json([
                'status' => 200,
                'data' => new OrderCheckBillResource($order),
                'change' => [
                    "money" => $amountMoneyReturn,
                    "quantity" => $listmoneyReturn
                ]
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'This order is closed.'
            ]);
        }

    }
}
