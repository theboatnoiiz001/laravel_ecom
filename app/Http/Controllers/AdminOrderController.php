<?php

namespace App\Http\Controllers;

use App\Http\Resources\AdminOrderPaidResource;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class AdminOrderController extends Controller
{
    //
    public function getPaidOrder(Request $request){
        $validator = Validator::make($request->all(),[
            'date_start' => 'required|date',
            'date_end' => 'required|date|after:date_start'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'data' => $validator->errors()
            ]);
        }

        $dateStart = Carbon::parse($request->date_start);
        $dateEnd = Carbon::parse($request->date_end);

        $order = Order::where('status',1)->whereBetween('updated_at',[$dateStart,$dateEnd])->get();
        if($order){
            return response()->json([
                'status' => 200,
                'data' => AdminOrderPaidResource::collection($order)
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'There are no outside paid orders during the selected period.'
            ]);
        }
        
    }

    public function getOrderByStatus(Request $request){
        $validator = Validator::make($request->all(),[
            'status' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'data' => $validator->errors()
            ]);
        }

        $order = Order::where('status',$request->status)->get();
        return response()->json([
            'status' => 200,
            'data' => AdminOrderPaidResource::collection($order)
        ]);
    }
}
