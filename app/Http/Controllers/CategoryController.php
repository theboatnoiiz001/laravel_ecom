<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    //
    public function index(){
        return response()->json([
            'status' => true,
            'data' => CategoryResource::collection(Category::all())
        ]);
    }

    public function create(Request $request){
        $validator = Validator($request->all(),[
            'name' => 'required|string|unique:categories,name|max:255',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ]);
        }

        $category = new Category();
        $category->name = $request->name;
        $category->save();

        return response()->json([
            'status' => true,
            'data' => new CategoryResource($category)
        ]);
    }
}
