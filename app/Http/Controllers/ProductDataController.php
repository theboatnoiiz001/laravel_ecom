<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderCheckBillResource;
use App\Http\Resources\ProductDataResource;
use App\Models\Order;
use App\Models\ProductData;
use Illuminate\Http\Request;

class ProductDataController extends Controller
{
    //
    public function create(Request $request){
        $validator = Validator($request->all(),[
            'product_id' => 'required|numeric|exists:products,id',
            'size' => 'required|string|max:255',
            'price' => 'required|numeric',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ]);
        }

        $productData = new ProductData();
        $productData->size = $request->size;
        $productData->price = $request->price;
        $productData->product_id = $request->product_id;
        $productData->save();


        return response()->json([
            'status' => 200,
            'data' => new ProductDataResource($productData)
        ]);
    }

    public function getDataByIdProduct($id){
        $dataProduct = ProductData::where('product_id',$id)->get();
        if($dataProduct){
            return response()->json([
                'status' => true,
                'data' => ProductDataResource::collection($dataProduct)
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Not found Product.'
            ]);
        }
    }
    
}
