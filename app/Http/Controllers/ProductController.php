<?php

namespace App\Http\Controllers;

use App\Http\Resources\FilterProductResource;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\ProductData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    //
    public function index(){
        return response()->json([
            'status' => true,
            'data' => ProductResource::collection(Product::all())
        ]);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'gender' => 'required|string|max:255',
            'category_id' => 'required|numeric|exists:categories,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ]);
        }

        $product = new Product();
        $product->name = $request->name;
        $product->gender = $request->gender;
        $product->category_id = $request->category_id;
        $product->save();


        return response()->json([
            'status' => true,
            'data' => new ProductResource($product)
        ]);
    }

    public function filterProduct(Request $request){
        $validator = Validator::make($request->all(),[
            'gender' => 'string|max:255|nullable',
            'category' => 'numeric|max:255|nullable',
            'size' => 'string|max:255|nullable'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()
            ]);
        }

        $product = DB::table('product_data')
                    ->join('products', 'product_data.product_id','=','products.id') 
                    ->select('products.*','product_data.*');
        
        if($request->gender){
            $product->where('products.gender',$request->gender);
        }

        if($request->category){
            $product->where('products.category_id',$request->category);
        }

        if($request->size){
            $product->where('product_data.size',$request->size);
        }

        $productFilter = $product->get();

        if($productFilter->count()){
            return response()->json([
                'status' => true,
                'data' => FilterProductResource::collection($productFilter)
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Not Found Product.'
            ]);
        }
    }
}
