<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderCheckBillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'order_id' => $this->id,
            'order_status' => $this->status,
            'employee_name' => $this->employee_name,
            'order_price' => $this->price,
            'order_detail' => OrderDetailResource::collection($this->orderDetail)
        ];
    }
}
